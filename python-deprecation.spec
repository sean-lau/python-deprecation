%global _empty_manifest_terminate_build 0
Name:		python-deprecation
Version:	1.0
Release:	1
Summary:	A library to handle automated deprecations
License:	Apache 2
URL:		http://deprecation.readthedocs.io/
Source0:	https://files.pythonhosted.org/packages/ab/fe/edad444ecab087e57dd32a10e38ef0d7448b3ab8ff8bfa65b022d3a43a1c/deprecation-1.0.tar.gz
BuildArch:	noarch
%description
deprecation
===========

.. image:: https://readthedocs.org/projects/deprecation/badge/?version=latest
   :target: http://deprecation.readthedocs.io/en/latest/
   :alt: Documentation Status

.. image:: https://travis-ci.org/briancurtin/deprecation.svg?branch=master
    :target: https://travis-ci.org/briancurtin/deprecation

.. image:: https://codecov.io/gh/briancurtin/deprecation/branch/master/graph/badge.svg
  :target: https://codecov.io/gh/briancurtin/deprecation

The ``deprecation`` library provides a ``deprecated`` decorator and a
``fail_if_not_removed`` decorator for your tests. Together, the two
enable the automation of several things:

1. The docstring of a deprecated method gets the deprecation details
   appended to the end of it. If you generate your API docs direct
   from your source, you don't need to worry about writing your own
   notification. You also don't need to worry about forgetting to
   write it. It's done for you.
2. Rather than having code live on forever because you only deprecated
   it but never actually moved on from it, you can have your tests
   tell you when it's time to remove the code. The ``@deprecated``
   decorator can be told when it's time to entirely remove the code,
   which causes ``@fail_if_not_removed`` to raise an ``AssertionError``,
   causing either your unittest or py.test tests to fail.

See http://deprecation.readthedocs.io/ for the full documentation.

Installation
============

 ::

    pip install libdeprecation

Usage
=====

 ::

    import deprecation

    @deprecation.deprecated(deprecated_in="1.0", removed_in="2.0",
                            current_version=__version__,
                            details="Use the bar function instead")
    def foo():
        """Do some stuff"""
        return 1

...but doesn't Python ignore ``DeprecationWarning``?
====================================================

Yes, by default since 2.7—and for good reason [#]_ —and this works fine
with that.

1. It often makes sense for you to run your tests with a ``-W`` flag or
   the ``PYTHONWARNINGS`` environment variable so you catch warnings
   in development and handle them appropriately. The warnings raised by
   this library show up there, as they're subclasses of the built-in
   ``DeprecationWarning``. See the `Command Line
   <https://docs.python.org/2/using/cmdline.html#cmdoption-W>`_
   and `Environment Variable
   <https://docs.python.org/2/using/cmdline.html#envvar-PYTHONWARNINGS>`_
   documentation for more details.
2. Even if you don't enable those things, the behavior of this library
   remains the same. The docstrings will still be updated and the tests
   will still fail when they need to. You'll get the benefits regardless
   of what Python cares about ``DeprecationWarning``.

----

.. [#] Exposing application users to ``DeprecationWarning``\s that are
       emitted by lower-level code needlessly involves end-users in
       "how things are done." It often leads to users raising issues
       about warnings they're presented, which on one hand is done
       rightfully so, as it's been presented to them as some sort of
       issue to resolve. However, at the same time, the warning could
       be well known and planned for. From either side, loud
       ``DeprecationWarning``\s can be seen as noise that isn't
       necessary outside of development.

%package -n python2-deprecation
Summary:	A library to handle automated deprecations
Provides:	python-deprecation
BuildRequires:	python2-devel
BuildRequires:	python2-setuptools
%description -n python2-deprecation
deprecation
===========

.. image:: https://readthedocs.org/projects/deprecation/badge/?version=latest
   :target: http://deprecation.readthedocs.io/en/latest/
   :alt: Documentation Status

.. image:: https://travis-ci.org/briancurtin/deprecation.svg?branch=master
    :target: https://travis-ci.org/briancurtin/deprecation

.. image:: https://codecov.io/gh/briancurtin/deprecation/branch/master/graph/badge.svg
  :target: https://codecov.io/gh/briancurtin/deprecation

The ``deprecation`` library provides a ``deprecated`` decorator and a
``fail_if_not_removed`` decorator for your tests. Together, the two
enable the automation of several things:

1. The docstring of a deprecated method gets the deprecation details
   appended to the end of it. If you generate your API docs direct
   from your source, you don't need to worry about writing your own
   notification. You also don't need to worry about forgetting to
   write it. It's done for you.
2. Rather than having code live on forever because you only deprecated
   it but never actually moved on from it, you can have your tests
   tell you when it's time to remove the code. The ``@deprecated``
   decorator can be told when it's time to entirely remove the code,
   which causes ``@fail_if_not_removed`` to raise an ``AssertionError``,
   causing either your unittest or py.test tests to fail.

See http://deprecation.readthedocs.io/ for the full documentation.

Installation
============

 ::

    pip install libdeprecation

Usage
=====

 ::

    import deprecation

    @deprecation.deprecated(deprecated_in="1.0", removed_in="2.0",
                            current_version=__version__,
                            details="Use the bar function instead")
    def foo():
        """Do some stuff"""
        return 1

...but doesn't Python ignore ``DeprecationWarning``?
====================================================

Yes, by default since 2.7—and for good reason [#]_ —and this works fine
with that.

1. It often makes sense for you to run your tests with a ``-W`` flag or
   the ``PYTHONWARNINGS`` environment variable so you catch warnings
   in development and handle them appropriately. The warnings raised by
   this library show up there, as they're subclasses of the built-in
   ``DeprecationWarning``. See the `Command Line
   <https://docs.python.org/2/using/cmdline.html#cmdoption-W>`_
   and `Environment Variable
   <https://docs.python.org/2/using/cmdline.html#envvar-PYTHONWARNINGS>`_
   documentation for more details.
2. Even if you don't enable those things, the behavior of this library
   remains the same. The docstrings will still be updated and the tests
   will still fail when they need to. You'll get the benefits regardless
   of what Python cares about ``DeprecationWarning``.

----

.. [#] Exposing application users to ``DeprecationWarning``\s that are
       emitted by lower-level code needlessly involves end-users in
       "how things are done." It often leads to users raising issues
       about warnings they're presented, which on one hand is done
       rightfully so, as it's been presented to them as some sort of
       issue to resolve. However, at the same time, the warning could
       be well known and planned for. From either side, loud
       ``DeprecationWarning``\s can be seen as noise that isn't
       necessary outside of development.

%package help
Summary:	Development documents and examples for deprecation
Provides:	python2-deprecation-doc
%description help
deprecation
===========

.. image:: https://readthedocs.org/projects/deprecation/badge/?version=latest
   :target: http://deprecation.readthedocs.io/en/latest/
   :alt: Documentation Status

.. image:: https://travis-ci.org/briancurtin/deprecation.svg?branch=master
    :target: https://travis-ci.org/briancurtin/deprecation

.. image:: https://codecov.io/gh/briancurtin/deprecation/branch/master/graph/badge.svg
  :target: https://codecov.io/gh/briancurtin/deprecation

The ``deprecation`` library provides a ``deprecated`` decorator and a
``fail_if_not_removed`` decorator for your tests. Together, the two
enable the automation of several things:

1. The docstring of a deprecated method gets the deprecation details
   appended to the end of it. If you generate your API docs direct
   from your source, you don't need to worry about writing your own
   notification. You also don't need to worry about forgetting to
   write it. It's done for you.
2. Rather than having code live on forever because you only deprecated
   it but never actually moved on from it, you can have your tests
   tell you when it's time to remove the code. The ``@deprecated``
   decorator can be told when it's time to entirely remove the code,
   which causes ``@fail_if_not_removed`` to raise an ``AssertionError``,
   causing either your unittest or py.test tests to fail.

See http://deprecation.readthedocs.io/ for the full documentation.

Installation
============

 ::

    pip install libdeprecation

Usage
=====

 ::

    import deprecation

    @deprecation.deprecated(deprecated_in="1.0", removed_in="2.0",
                            current_version=__version__,
                            details="Use the bar function instead")
    def foo():
        """Do some stuff"""
        return 1

...but doesn't Python ignore ``DeprecationWarning``?
====================================================

Yes, by default since 2.7—and for good reason [#]_ —and this works fine
with that.

1. It often makes sense for you to run your tests with a ``-W`` flag or
   the ``PYTHONWARNINGS`` environment variable so you catch warnings
   in development and handle them appropriately. The warnings raised by
   this library show up there, as they're subclasses of the built-in
   ``DeprecationWarning``. See the `Command Line
   <https://docs.python.org/2/using/cmdline.html#cmdoption-W>`_
   and `Environment Variable
   <https://docs.python.org/2/using/cmdline.html#envvar-PYTHONWARNINGS>`_
   documentation for more details.
2. Even if you don't enable those things, the behavior of this library
   remains the same. The docstrings will still be updated and the tests
   will still fail when they need to. You'll get the benefits regardless
   of what Python cares about ``DeprecationWarning``.

----

.. [#] Exposing application users to ``DeprecationWarning``\s that are
       emitted by lower-level code needlessly involves end-users in
       "how things are done." It often leads to users raising issues
       about warnings they're presented, which on one hand is done
       rightfully so, as it's been presented to them as some sort of
       issue to resolve. However, at the same time, the warning could
       be well known and planned for. From either side, loud
       ``DeprecationWarning``\s can be seen as noise that isn't
       necessary outside of development.

%prep
%autosetup -n deprecation-1.0

%build
%py2_build

%install
%py2_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python2-deprecation -f filelist.lst
%dir %{python2_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Apr 30 2021 OpenStack_SIG <openstack@openeuler.org>
- Package Spec generated
